﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace NoteApp.Models
{
    public class NoteModel
    {
        public int Id { get; set; }
        [DisplayName("Тема")]
        public string Topic { get; set; }
        [DisplayName("Сообщение")]
        public string Message { get; set; }
        [DisplayName("Клиент")]
        public string ClientName { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Дата")]
        public DateTime NoteDate { get; set; }
        [DisplayName("Отвечено")]
        public bool IsAnswered { get; set; }
    }
}