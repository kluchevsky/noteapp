﻿using System;
using System.Runtime.Serialization;

namespace NoteApp.Exceptions
{
    [Serializable]
    class LimitTimeException : Exception
    {
        public LimitTimeException()
        {
        }

        public LimitTimeException(string message) : base(message)
        {
        }

        public LimitTimeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LimitTimeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}