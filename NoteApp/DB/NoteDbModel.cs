﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoteApp.DB
{
    public class NoteDbModel
    {
        public int Id { get; set; }
        public string Topic { get; set; }
        public string Message { get; set; }
        public string ClientName { get; set; }
        public string Email { get; set; }
        public DateTime NoteDate { get; set; }
        public bool IsAnswered { get; set; }
    }
}