﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NoteApp.DB
{
    public class NoteDbContext : DbContext
    {
        public NoteDbContext() : base("DefaultConnection") { }

        public virtual DbSet<NoteDbModel> Notes { get; set; }
    }
}