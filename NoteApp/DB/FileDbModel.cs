﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoteApp.DB
{
    public class FileDbModel
    {
        public int Id { get; set; }
        public int NoteId { get; set; }
        public string FileName { get; set; }
        public byte[] FileData { get; set; }
    }
}