﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace NoteApp.DB
{
    public class FileRepository
    {
        public async Task SaveFile(FileDbModel item, CancellationToken cancelToken)
        {
            using (var context = new FileDbContext())
            {
                context.Files.Add(item);

                await context.SaveChangesAsync(cancelToken);
            }
        }

        public async Task<FileDbModel> GetFile(int noteId, CancellationToken cancelToken)
        {
            using (var context = new FileDbContext())
            {
                return await context.Files.FirstOrDefaultAsync(f => f.NoteId == noteId);
            }
        }

    }
}