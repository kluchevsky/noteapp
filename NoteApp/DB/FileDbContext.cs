﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NoteApp.DB
{
    public class FileDbContext : DbContext
    {
        public FileDbContext() : base("DefaultConnection") { }

        public virtual DbSet<FileDbModel> Files { get; set; }
    }
}