﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace NoteApp.DB
{
    public class NoteRepository
    {
        public async Task<NoteDbModel> CreateNote(NoteDbModel item, CancellationToken cancelToken)
        {
            using (var context = new NoteDbContext())
            {
                context.Notes.Add(item);

                await context.SaveChangesAsync(cancelToken);

                return item;
            }
        }

        public List<NoteDbModel> GetAll()
        {
            using (var context = new NoteDbContext())
            {
                return context.Notes.ToList();
            }
        }

        public async Task<NoteDbModel> GetLastNote(string user, DateTime limitTime, CancellationToken cancelToken)
        {
            using (var context = new NoteDbContext())
            {                
                return await context.Notes.FirstOrDefaultAsync(n => (n.ClientName == user) && (n.NoteDate>limitTime), cancelToken);
            }
        }

        public async Task SetFlag(int id, bool flag, CancellationToken cancelToken)
        {
            using (var context = new NoteDbContext())
            {
                var item = await context.Notes.FirstOrDefaultAsync(n => n.Id == id);
                item.IsAnswered = flag;
                await context.SaveChangesAsync(cancelToken);
            }
        }

    }
}