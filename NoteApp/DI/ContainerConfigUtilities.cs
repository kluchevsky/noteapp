﻿using Autofac;
using Autofac.Integration.Mvc;
using NoteApp.DB;
using NoteApp.Services;
using System.Web.Mvc;

namespace NoteApp.DI
{
    public class ContainerConfigUtilities
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            // регистрируем контроллер в текущей сборке
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // регистрируем споставление типов
            builder.RegisterType<NotesService>().As<INotesService>();
            builder.RegisterType<CheckRoleService>().As<ICheckRoleService>();
            builder.RegisterType<NoteApp.Services.EmailService>().As<IEmailService>();           

            builder.RegisterType<NoteRepository>();
            builder.RegisterType<FileRepository>();

            // создаем новый контейнер 
            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}