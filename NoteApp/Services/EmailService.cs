﻿using NoteApp.DB;
using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace NoteApp.Services
{
    public class EmailService : IEmailService
    {
        private string EmailAccount { get; set; }
        private string EmailPassword { get; set; }
        private string EmailHost { get; set; }
        private int EmailPort { get; set; }
        private bool EmailSsl { get; set; }

        public EmailService() {

            EmailAccount = ConfigurationManager.AppSettings["EmailAccount"];
            EmailPassword = ConfigurationManager.AppSettings["EmailPassword"];
            EmailHost = ConfigurationManager.AppSettings["EmailHost"];
            EmailPort = Int32.Parse(ConfigurationManager.AppSettings["EmailPort"]);
            EmailSsl = ConfigurationManager.AppSettings["EmailSsl"] == "true";
        }

        public void SendCreateEmail(NoteDbModel note) {
            Task.Run(() => SendEmail("New message", 
                "id = "+ note.Id+", Тема: "+ note.Topic));
        }

        public void SendProcessEmail(int id, bool flag)
        {
            Task.Run(() => SendEmail("Message processed",
                "id = " + id + ", Результат: " + flag));
        }

        private void SendEmail(string subject, string messageBody)
        {
            try
            {
                var message = GetMessage(subject, messageBody);
                SendMessage(message);
            }
            catch (Exception) { }
        }

        private MailMessage GetMessage(string subject, string messageBody) {
            MailMessage message = new MailMessage
            {
                From = new MailAddress(EmailAccount)
            };
            message.To.Add(new MailAddress(EmailAccount));
            message.Subject = subject;
            message.IsBodyHtml = false; //to make message body as html  
            message.Body = messageBody;

            return message;
        }

        private void SendMessage(MailMessage message)
        {
            SmtpClient smtp = new SmtpClient
            {
                Port = EmailPort,
                Host = EmailHost, 
                EnableSsl = EmailSsl,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(EmailAccount, EmailPassword),
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            smtp.Send(message);
        }
    }
}