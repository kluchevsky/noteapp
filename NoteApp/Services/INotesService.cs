﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using NoteApp.DB;
using NoteApp.Models;

namespace NoteApp.Services
{
    public interface INotesService
    {
        Task AddNoteAsync(NoteModel note, string user, HttpPostedFileBase upload);
        Task ChangeFlagAsync(int noteId, bool flag);
        List<NoteModel> GetAllNotes();
        Task<FileDbModel> GetNoteFileAsync(int noteId);
    }
}