﻿using NoteApp.Exceptions;
using NoteApp.DB;
using NoteApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.IO;

namespace NoteApp.Services
{
    public class NotesService : INotesService
    {
        private readonly NoteRepository _noteRepository;
        private readonly FileRepository _fileRepository;
        private readonly IEmailService _emailService;

        public NotesService(NoteRepository noteRepository, FileRepository fileRepository, IEmailService emailService)
        {
            _noteRepository = noteRepository;
            _fileRepository = fileRepository;
            _emailService = emailService;
        }

        public List<NoteModel> GetAllNotes() {

            return _noteRepository.GetAll().Select(d => new NoteModel() {
                Id = d.Id,
                Topic = d.Topic,
                Message = d.Message,
                ClientName = d.ClientName,
                Email = d.Email,
                NoteDate = d.NoteDate,
                IsAnswered = d.IsAnswered
            }).ToList();
        }

        public async Task AddNoteAsync(NoteModel note, string user, HttpPostedFileBase upload)
        {
            var lastNote = await _noteRepository.GetLastNote(user, DateTime.Now.AddHours(-24), CancellationToken.None);

            if (lastNote != null) {
                throw new LimitTimeException();
            }

            var item = new NoteDbModel() {
                Topic = note.Topic,
                Message = note.Message,
                ClientName = user,
                Email = user,
                NoteDate = DateTime.Now,
                IsAnswered = false
            };
            
            var savedItem = await _noteRepository.CreateNote(item, CancellationToken.None);

            if (upload != null) {
                var noteFile = new FileDbModel()
                {
                    NoteId = savedItem.Id,
                    FileName = upload.FileName,
                    FileData = ConvertToByte(upload)
                };

                await _fileRepository.SaveFile(noteFile, CancellationToken.None);
            }

            _emailService.SendCreateEmail(savedItem);
        }

        private byte[] ConvertToByte(HttpPostedFileBase file)
        {
            byte[] fileByte = null;
            BinaryReader rdr = new BinaryReader(file.InputStream);
            fileByte = rdr.ReadBytes((int)file.ContentLength);
            return fileByte;
        }

        public async Task ChangeFlagAsync(int noteId, bool flag) {
            await _noteRepository.SetFlag(noteId, flag, CancellationToken.None);
            _emailService.SendProcessEmail(noteId, flag);
        }

        public async Task<FileDbModel> GetNoteFileAsync(int noteId) {
            var noteFile = await _fileRepository.GetFile(noteId, CancellationToken.None);
            return noteFile;
        }
    }
}