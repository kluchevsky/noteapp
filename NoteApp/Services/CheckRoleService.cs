﻿using System.Security.Principal;

namespace NoteApp.Services
{
    public class CheckRoleService : ICheckRoleService
    {
        public bool IsManager(IPrincipal user) {
            return user.IsInRole("Manager");
        }
    }
}