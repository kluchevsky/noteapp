﻿using NoteApp.DB;
using NoteApp.Models;

namespace NoteApp.Services
{
    public interface IEmailService
    {
        void SendCreateEmail(NoteDbModel noteModel);
        void SendProcessEmail(int id, bool flag);
    }
}