﻿using System.Security.Principal;

namespace NoteApp.Services
{
    public interface ICheckRoleService
    {
        bool IsManager(IPrincipal user);
    }
}