﻿using NoteApp.Exceptions;
using NoteApp.Models;
using NoteApp.Services;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NoteApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly INotesService _notesService;
        private readonly ICheckRoleService _checkRoleService;        

        public HomeController(INotesService notesService, ICheckRoleService checkRoleService) {
            _notesService = notesService;
            _checkRoleService = checkRoleService;
        }

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return _checkRoleService.IsManager(User) ? View("NoteListView", _notesService.GetAllNotes()) : View("CreateNoteView");
            }
            return RedirectToAction("Login", "Account");            
            
        }

        public ActionResult NoteListView() {
            return Index();
        }

        public ActionResult CreateNoteView()
        {
            return Index();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(NoteModel item, HttpPostedFileBase upload)
        {
            try
            {
                await _notesService.AddNoteAsync(item, User.Identity.Name, upload);
            }
            catch (LimitTimeException)
            {
                ModelState.AddModelError("NoteDate", "Допускается одно сообщение в течение 24 часов");
            }
            return View("CreateNoteView");
        }

        public async Task<ActionResult> GetFile(int id)
        {
            var file = await _notesService.GetNoteFileAsync(id);
            if (file == null) return Index();
            return File(file.FileData, System.Net.Mime.MediaTypeNames.Application.Octet, file.FileName);
        }

    }
}