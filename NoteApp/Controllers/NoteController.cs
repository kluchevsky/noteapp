﻿using NoteApp.Services;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NoteApp.Controllers
{
    public class NoteController : Controller
    {
        private readonly INotesService _notesService;

        public NoteController(INotesService notesService)
        {
            this._notesService = notesService;
        }

        public async Task SetFlag(int id, bool flag)
        {
            await _notesService.ChangeFlagAsync(id, flag);
        }

    }
}